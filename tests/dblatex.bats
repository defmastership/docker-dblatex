#!/usr/bin/env bats

TMP_GENERATION_DIR="${BATS_TEST_DIRNAME}/tmp"
DBLATEX_VERSION=0.3.12
DOCKER_IMAGE_NAME_TO_TEST="dblatex"
TEXLIVE_VERSION=2022.20230122-3
INKSCAPE_VERSION=1.2.2-2

clean_generated_files() {
  docker run -t --rm -v "${BATS_TEST_DIRNAME}:${BATS_TEST_DIRNAME}" debian \
    rm -rf "${TMP_GENERATION_DIR}"
}

setup() {
  clean_generated_files
  mkdir -p "${TMP_GENERATION_DIR}"
}

teardown() {
  clean_generated_files
}

@test "The Docker image to test is available" {
  docker inspect "${DOCKER_IMAGE_NAME_TO_TEST}"
}

@test "dblatex is installed and in version ${DBLATEX_VERSION}" {
  docker run -t --rm "${DOCKER_IMAGE_NAME_TO_TEST}" dblatex --version \
    | grep "dblatex" | grep "${DBLATEX_VERSION}"
}

@test "Timezone data is present in the image" {
  docker run -t --rm "${DOCKER_IMAGE_NAME_TO_TEST}" test -f /usr/share/zoneinfo/posixrules
}

@test "make is installed and in the path" {
  docker run -t --rm "${DOCKER_IMAGE_NAME_TO_TEST}" which make
}

@test "rake is installed and in the path" {
  docker run -t --rm "${DOCKER_IMAGE_NAME_TO_TEST}" which rake
}

@test "curl is installed and in the path" {
  docker run -t --rm "${DOCKER_IMAGE_NAME_TO_TEST}" which curl
}

@test "bash is installed and in the path" {
  docker run -t --rm "${DOCKER_IMAGE_NAME_TO_TEST}" which bash
}

@test "python3 is installed, in the path, and executable" {
  docker run -t --rm "${DOCKER_IMAGE_NAME_TO_TEST}" which python3
  docker run -t --rm "${DOCKER_IMAGE_NAME_TO_TEST}" python3 --version
}

@test "git command line tool is installed and in the path" {
  docker run -t --rm "${DOCKER_IMAGE_NAME_TO_TEST}" which git
  docker run -t --rm "${DOCKER_IMAGE_NAME_TO_TEST}" git --version
}

@test "We can generate a PDF document from basic example" {
  docker run -t --rm \
    -v "${BATS_TEST_DIRNAME}":/documents/ \
    "${DOCKER_IMAGE_NAME_TO_TEST}" \
      dblatex -p /documents/fixtures/example_param.xsl \
      -o /documents/tmp/stdf_manual.pdf \
      /documents/fixtures/stdf_manual.xml
}

@test "texlive is installed to render pdf from dockbook" {
  docker run -t --rm "${DOCKER_IMAGE_NAME_TO_TEST}" dpkg --no-pager --list texlive  \
    | grep "texlive" | grep "${TEXLIVE_VERSION}"
}

@test "inkscape is installed to transform svg files" {
  docker run -t --rm "${DOCKER_IMAGE_NAME_TO_TEST}" dpkg --no-pager --list inkscape  \
    | grep "inkscape" | grep "${INKSCAPE_VERSION}"
}
