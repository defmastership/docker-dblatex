ARG debian_version=bookworm-slim
FROM debian:${debian_version} AS base

# Mettre à jour la liste des paquets et installer les outils de base
    RUN apt-get update && apt-get install -y \
    apt-transport-https \
    ca-certificates \
    curl \
    git \
    wget

# Supprimer les paquets inutiles
RUN apt-get purge -y \
    *apparmor* \
    *cups* \
    *firmware-atheros* \
    *firmware-linux* \
    *geoclue* \
    *libcups* \
    *libdbus-glib-1-dev* \
    *libgl1-mesa-dev* \
    *libice-dev* \
    *libmng-dev* \
    *libpq-dev* \
    *libpulse-dev* \
    *libsqlite3-dev* \
    *libtool* \
    *libx11-dev* \
    *libxext-dev* \
    *libxml2-dev* \
    *manpages-dev* \
    *xserver-xorg*

# Supprimer les fichiers de cache
    RUN apt-get autoremove -y

FROM base AS main
    RUN apt-get update && apt-get install -y \
    texlive \
    texlive-font-utils \
    inkscape \
    make \
    dblatex

    WORKDIR /documents
    VOLUME /documents

    CMD ["/bin/bash"]
