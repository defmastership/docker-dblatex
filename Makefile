
export DOCKER_BUILDKIT=1
GIT_TAG = $(shell git describe --exact-match --tags HEAD 2>/dev/null)
ifeq ($(strip $(GIT_TAG)),)
GIT_REF = $(shell git rev-parse --abbrev-ref HEAD 2>/dev/null)
else
GIT_REF = $(GIT_TAG)
endif
ARCH = $(shell uname -m)
LOCAL_TARGET = $(shell if [ $(ARCH) = "aarch64" ] || [ $(ARCH) = "arm64" ]; then echo "linux/arm64"; else echo "linux/amd64"; fi)
BUILDER = $(shell if $$(docker buildx use dblatex 2> /dev/null) ; then echo "true"; else echo "false"; fi)

all: build test 
all-load: build-load test 

build-load: builder-init dblatex.build-load
build: build-load dblatex.build

%.build-load:
	docker buildx bake $(*) --set *.platform=$(LOCAL_TARGET) --load --builder=dblatex --print
	docker buildx bake $(*) --set *.platform=$(LOCAL_TARGET) --load --builder=dblatex

%.build:
	docker buildx bake $(*) --builder=dblatex --print
	docker buildx bake $(*) --builder=dblatex

test: dblatex.test

%.test:
	bats $(CURDIR)/tests/$(*).bats

deploy: dblatex.deploy

%.deploy:
	docker buildx bake $(*) --push --builder=dblatex --print
	docker buildx bake $(*) --push --builder=dblatex

builder-init:
	if [ $(BUILDER) = false ]; then docker buildx create --name dblatex --driver docker-container --use && docker buildx inspect --bootstrap; fi

clean:
	rm -rf "$(CURDIR)/cache"

cache:
	mkdir -p "$(CURDIR)/cache"

.PHONY: all build build-load builder-init test deploy clean docker-cache
