variable "IMAGE_VERSION" {
  default = ""
}

variable "IMAGE_NAME" {
  default = "dblatex"
}

group "all" {
  targets = [
    "dblatex"
  ]
}

target "dblatex" {
  dockerfile = "Dockerfile"
  context    = "."
  target     = "main"
  platforms  = ["linux/amd64"]
  tags       = [
    "${IMAGE_NAME}",
    notequal("", IMAGE_VERSION) ? "${IMAGE_NAME}:${IMAGE_VERSION}" : "", // Only used when deploying on a tag
    notequal("", IMAGE_VERSION) ? "${IMAGE_NAME}:${element(split(".", IMAGE_VERSION), 0)}" : "", // Only used when deploying on a tag (1 digit)
    notequal("", IMAGE_VERSION) ? "${IMAGE_NAME}:${element(split(".", IMAGE_VERSION), 0)}.${element(split(".", IMAGE_VERSION), 1)}" : "", // Only used when deploying on a tag (2 digits)
  ]
}
